package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.demo.model.registration;
import com.example.demo.repo.studentrepo;

@Service
public class studentservice {

	@Autowired
	studentrepo rep;
	
	public registration addstudentservice(registration reg) {
		return this.rep.save(reg);
		
	}
}
