package com.example.demo.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.registration;
import com.example.demo.service.studentservice;

@CrossOrigin(origins ="http://localhost:4200/")
@RestController
@RequestMapping("/student") 
public class Control {

	@Autowired
	studentservice service;
	
	@PostMapping("/registration")
	public registration adddata(@RequestBody registration regi) {
		return this.service.addstudentservice(regi);
		
		
	}
	
}
